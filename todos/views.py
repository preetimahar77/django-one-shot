from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {"todolist": todo_list_list}
    return render(request, "todolists/list.html", context)


def show_todo_list_detail(request, id):
    model_instance = TodoList.objects.get(id=id)
    context = {"model_instance": model_instance}
    return render(request, "todolists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)

        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoListForm()
        context = {"form": form}

    return render(request, "todolists/create.html", context)


def todo_list_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoListForm(instance=model_instance)
    context = {"form": form}
    return render(request, "todolists/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todolists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)

        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)

    else:
        form = TodoItemForm()
        todo_list_list = TodoList.objects.all()
        context = {"form": form, "todo_lists": todo_list_list}

    return render(request, "todoitems/create.html", context)


def todo_item_update(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm(instance=model_instance)
        todo_list_list = TodoList.objects.all()
        context = {
            "form": form,
            "todo_item": model_instance,
            "todo_lists": todo_list_list,
        }
        return render(request, "todoitems/edit.html", context)
